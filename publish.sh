#!/bin/bash

#set -eo pipefail

cd /code/code-generation/

if [ -e ./nohup.out ]; then
  rm -rf ./nohup.out
fi

echo "process start ... " >> ./nohup.out

echo "setp 1/5: git pull" >> ./nohup.out
git pull > pullResult.text

# 如果没有代码更新，则不部署
if [ -e ./pullResult.text ]; then
    pullR=`cat pullResult.text`
    if [ `echo $pullR | sed s/[[:space:]]//g` = "Alreadyuptodate." ];then
        echo "Already up to date." >> ./nohup.out
        exit 0
    fi
fi

echo "setp 2/5: kill pidfile.txt" >> ./nohup.out
if [ -e ./pidfile.txt ]; then
    runingPid=`cat pidfile.txt`
    if ps -p $runingPid > /dev/null
    then
       echo "kill -9 $runingPid" >> ./nohup.out
       kill -9 $runingPid
    fi
fi

# 编译项目
echo "setp 3/5: mvn package" >> ./nohup.out
mvn clean package -Dmaven.test.skip=true >> ./nohup.out

echo "setp 4/5: run jar" >> ./nohup.out
nohup java -jar ./target/code-generator-1.0.0.jar & echo $! > pidfile.txt

echo "finshed." >> ./nohup.out
