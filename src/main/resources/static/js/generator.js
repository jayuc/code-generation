$(function () {
    $("#jqGrid").jqGrid({
        url: 'sys/generator/list',
        datatype: "json",
        colModel: [			
			{ label: '表名', name: 'tableName', width: 100, key: true },
			{ label: 'Engine', name: 'engine', width: 70},
			{ label: '菜单编号', name: 'menuCode', width: 70, formatter: function (value, opt, row) {
                if(!value){
                    return "<span t=\"" + row.tableName + "\"><input type='text' />" +
                        "<button class='cellt' style='margin-left: 2px'>保存</button></span>";
                }
                return value;
            } },
            { label: '表备注', name: 'tableComment', width: 100 },
			{ label: '创建时间', name: 'createTime', width: 100 }
        ],
		viewrecords: true,
        height: 650,
        rowNum: 15,
		rowList : [15,30,50,100,200],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        	$(".cellt").click(function (e) {
                e.stopPropagation();
                var tableName = $(e.target).parent().attr('t');
                var mc = $(e.target).siblings('input').val();
                $.ajax({
                    url: '/setting/setMenu',
                    data: {tableName: tableName, menuCode: mc},
                    success: function () {
                        $(e.target).parent().html(mc);
                    },
                    fail: function (e) {
                        console.log(e);
                    }
                })
            });

        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			tableName: null
		},
        moduleName: null,
        moduleResult : '--'
	},
	methods: {
		query: function () {
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'tableName': vm.q.tableName},
                page:1 
            }).trigger("reloadGrid");
		},
		generator: function() {
            var tableNames = getSelectedRows();
            if(tableNames == null){
                return ;
            }
            location.href = "sys/generator/code?tables=" + tableNames.join();
		},
        setMoudle: function () {
            var name = this.moduleName;
            var that = this;
            $.ajax({
                url: '/setting/setModule',
                data: {name: name},
                success: function (result) {
                    console.log(' -------> success', result);
                    that.moduleResult = name;
                    that.moduleName = null;
                },
                fail: function (e) {
                    console.log(e);
                }
            })
        }
	}
});



