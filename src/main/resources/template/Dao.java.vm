package ${package}.${moduleName}.mapper;

import ${package}.${moduleName}.entity.${className};
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * ${comments}
 * 
 * @author ${author}
 * @email ${email}
 * @date ${datetime}
 */
@Mapper
public interface ${className}Mapper {

    @Select({
            "<script> ",
            "select t.* ",
            "  from ${tableName} t ",
            "  <where>  ",

            "  </where > ",
            "</script> "
    })
    List<${className}> list(${className} record);

    @Insert("insert into ${tableName} (" +
#foreach ($column in $columnGroupList)
            "      $column " +
#end
            "    ) values (" +
#foreach ($attr in $attrGroupList)
            "      ${CreateTimeUtil.handleAttrByRecordAdd($attr)} " +
#end
            "      )")
    @Options(useGeneratedKeys = true, keyProperty = "record.${pkAttr}")
    Integer add(@Param("record") ${className} record);

    @Insert({
            "<script> ",
            "insert into ${tableName} (",
#foreach ($column in $columnGroupList)
            "      $column ",
#end
            "    ) values ",
            "   <foreach collection=\"list\" separator=\",\" item=\"item\">",
            "       (",
#foreach ($attr in $attrGroupList)
            "      ${CreateTimeUtil.handleAttrByBatchAdd($attr)} ",
#end
            "      )",
            "   </foreach>",
            "</script> "
    })
    Integer batchAdd(@Param("list") List<${className}> list);

    @Update({
            "<script> ",
            "update ${tableName} set ",
#foreach ($updates in $columnUpdateList)
            "       $updates ",
#end
            "where ${pkName} = #{${pkAttr}}",
            "</script> "
    })
    Integer update(${className} record);

    @Update({
            "<script> ",
            "update ${tableName} ",
            "   <set> ",
#foreach ($updates in $columnUpdateSelectList)
            "       $updates ",
#end
            "   </set> ",
            "where ${pkName} = #{${pkAttr}}",
            "</script> "
    })
    Integer updateSelect(${className} record);

    @Delete("delete from ${tableName} where ${pkName} = #{id}")
    Integer delete(Integer id);

    @Delete({
            "<script> ",
            "delete from ${tableName} where ${pkName} in ",
            "   <foreach collection=\"ids\" item=\"item\" index=\"index\" open=\"(\" close=\")\" separator=\",\">",
            "            #{item}",
            "   </foreach>",
            "</script> "
    })
    Integer batchDelete(@Param("ids") List<Integer> ids);
	
}
