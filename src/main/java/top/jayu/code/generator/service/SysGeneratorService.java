/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package top.jayu.code.generator.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import top.jayu.code.generator.config.MongoManager;
import top.jayu.code.generator.dao.GeneratorDao;
import top.jayu.code.generator.dao.MongoDBGeneratorDao;
import top.jayu.code.generator.factory.MongoDBCollectionFactory;
import top.jayu.code.generator.utils.GenUtils;
import top.jayu.code.generator.utils.PageUtils;
import top.jayu.code.generator.utils.Query;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成器
 *
 * @author Mark sunlightcs@gmail.com
 */
@Service
public class SysGeneratorService {

    @Autowired
    private GeneratorDao generatorDao;

    static Map<Object, Object> menuMap = new HashMap<>();

    // module name
    static String moduleName = null;

    public Integer setMenu(String tableName, String menuCode){
        menuMap.put(tableName, menuCode);
        return 1;
    }

    public void setModuleName(String name){
        moduleName = name;
    }

    public PageUtils queryList(Query query) {
        Page<?> page = PageHelper.startPage(query.getPage(), query.getLimit());
        List<Map<String, Object>> list = generatorDao.queryList(query);
        for (Map<String, Object> item:list){
            Object tableName = item.get("tableName");
            if(menuMap.containsKey(tableName)){
                item.put("menuCode", menuMap.get(tableName));
            }else {
                item.put("menuCode", "");
            }
        }
        int total = (int) page.getTotal();
        if (generatorDao instanceof MongoDBGeneratorDao) {
            total = MongoDBCollectionFactory.getCollectionTotal(query);
        }
        return new PageUtils(list, total, query.getLimit(), query.getPage());
    }

    public Map<String, String> queryTable(String tableName) {
        return generatorDao.queryTable(tableName);
    }

    public List<Map<String, String>> queryColumns(String tableName) {
        return generatorDao.queryColumns(tableName);
    }


    public byte[] generatorCode(String[] tableNames) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        for (String tableName : tableNames) {
            //查询表信息
            Map<String, String> table = queryTable(tableName);
            //查询列信息
            List<Map<String, String>> columns = queryColumns(tableName);
            //生成代码
            GenUtils.generatorCode(table, columns, zip, moduleName, (String) menuMap.get(tableName));
        }
        if (MongoManager.isMongo()) {
            GenUtils.generatorMongoCode(tableNames, zip);
        }


        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }
}
