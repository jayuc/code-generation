package top.jayu.code.generator.utils;

import org.apache.commons.lang.StringUtils;

public class CreateTimeUtil {

    public static String handleAttrByAdd(String str){
        return str.replace("#{createTime}", "now()");
    }

    public static String handleAttrByBatchAdd(String str){
        return str.replace("#{createTime}", "now()").replace("#{", "#{item.");
    }

    public static String handleAttrByRecordAdd(String str){
        return str.replace("#{createTime}", "now()").replace("#{", "#{record.");
    }

    public static String generateParentCode(String menuCode){
        if(StringUtils.isNotBlank(menuCode)){
            return menuCode.substring(0, menuCode.length()-2);
        }
        return "";
    }

    public static Integer generateSort(String menuCode){
        if(StringUtils.isNotBlank(menuCode)){
            return Integer.valueOf(menuCode.substring(menuCode.length()-2, menuCode.length()));
        }
        return 1;
    }

    public static String generatePath(String tableName){
        return tableName.replace("t_", "").replace("_", "-");
    }

    public static String generateU(String tableName){
        return tableName.replace("t_", "").replace("_", "/");
    }

}
