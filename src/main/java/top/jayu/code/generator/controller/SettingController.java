package top.jayu.code.generator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.jayu.code.generator.service.SysGeneratorService;

@RestController
@RequestMapping("/setting")
public class SettingController {

    @Autowired
    SysGeneratorService sysGeneratorService;

    @GetMapping("/setMenu")
    public Object setMenu(String tableName, String menuCode){
        return sysGeneratorService.setMenu(tableName, menuCode);
    }

    @GetMapping("/setModule")
    public Integer setModule(String name){
        sysGeneratorService.setModuleName(name);
        return 1;
    }

}
